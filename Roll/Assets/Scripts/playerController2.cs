﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;

public class playerController2 : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    //Rigidbody gives the playerObject physics
    Rigidbody rb;
    private int count;

    // Use this for initialization
    void Start()
    {
        //Adds physics to player
        rb = GetComponent<Rigidbody>();
        //initializes default count
        count = 0;
        SetCountText();
        //Hides win text
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        //Finds out which arrow keys are being pressed?
        float moveHorizantal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        //saves pressed directions to a vector.
        Vector3 movement = new Vector3(moveHorizantal, 0f, moveVertical);
        //Allows player to move
        rb.AddForce(movement * speed);

    }

    //Increases count when player collides with a Pickup object, then causes the Pickup object to disappear.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {   //causes object 
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
    }
    //Increases score in the top left corner of screen. Also displays a win message if the count reaches 12.
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 39)
        {
            //Displays win text
            winText.text = "You Win!";
           

        }
    }
    
}
