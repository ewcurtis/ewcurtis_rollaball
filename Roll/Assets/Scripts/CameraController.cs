﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;

    private Vector3 offset;

	// Use this for initialization
	void Start () {
        // Keeps the camera its default distance away from the player 
        offset = transform.position - player.transform.position;


	}
	
	// Update is called once per frame
	void LateUpdate () {
        //Lets the camera follow the player around
        transform.position = player.transform.position + offset;

	}
}
