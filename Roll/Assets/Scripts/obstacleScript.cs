﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class obstacleScript : MonoBehaviour {

    Rigidbody rb;
    public float pushSpeed;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
       

    }

    void FixedUpdate()
    {
        //Obstacle will go upwards every other second
        if (Mathf.RoundToInt(timer) % 2 != 0 && Mathf.RoundToInt(timer) % 1 == 0)
        {
        rb.velocity = transform.up * pushSpeed;
        }
        //Obstacle will go downwards every other second
        if (Mathf.RoundToInt(timer) % 2 == 0)
        {
            
            rb.velocity = -transform.up * pushSpeed;
        }
    }
}
